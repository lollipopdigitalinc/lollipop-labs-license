Lollipop Labs License - L³
========================

---

See `EXAMPLE.txt` for an example of a completed license document. Once there is a stable version there should be a `LICENSE.txt` file in here to use.

---

Purpose
-------
The Lollipop Labs License is a software/project license that strives to provide a combination FOSS and commercial license for which the lab team members and company feel there is a good compromise.

Use
---
Besides being used in our labs, we hope that the L³ agreement can be used by other corporate labs to fit their own needs.
